"""A module for testing the EnzymeCostSimulator class."""

# The MIT License (MIT)
#
# Copyright (c) 2013 Weizmann Institute of Science
# Copyright (c) 2018-2020 Institute for Molecular Systems Biology,
# ETH Zurich
# Copyright (c) 2018-2020 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import numpy as np
import pytest
from equilibrator_api import Q_

from equilibrator_pathway import EnzymeCostFunction, EnzymeCostSimulator


@pytest.fixture(scope="module")
def bistable_ecf() -> EnzymeCostFunction:
    """Create a model with bistability."""
    Nc = 3
    Nr = 3

    S = np.zeros((Nc, Nr))
    fluxes = np.zeros((Nr, 1)) * Q_("mM/s")
    kcat = np.zeros((Nr, 1)) * Q_("1/s")
    standard_dg = np.zeros((Nr, 1)) * Q_("kJ/mol")
    KMM = np.ones((Nc, Nr)) * Q_("M")
    A_act = np.zeros((Nc, Nr))
    K_act = np.ones((Nc, Nr)) * Q_("M")
    A_inh = np.zeros((Nc, Nr))
    K_inh = np.ones((Nc, Nr)) * Q_("M")

    # v0: X0 -> X1
    S[0, 0] = -1.0
    S[1, 0] = 1.0
    fluxes[0, 0] = Q_(2.0, "mM/s")
    kcat[0, 0] = Q_(20.0, "1/s")
    standard_dg[0, 0] = Q_(-30, "kJ/mol")
    KMM[0, 0] = Q_(1e-2, "M")
    KMM[1, 0] = Q_(1e-4, "M")

    # v1: X1 -> X2
    S[1, 1] = -1.0
    S[2, 1] = 1.0
    fluxes[1, 0] = Q_(1.0, "mM/s")
    kcat[1, 0] = Q_(8.0, "1/s")
    standard_dg[1, 0] = Q_(-20, "kJ/mol")
    KMM[1, 1] = Q_(1e-4, "M")
    KMM[2, 1] = Q_(1e-1, "M")

    # v2: X1 -> X2
    S[1, 2] = -1.0
    S[2, 2] = 1.0
    fluxes[2, 0] = Q_(1.0, "mM/s")
    kcat[2, 0] = Q_(0.1, "1/s")
    standard_dg[2, 0] = Q_(-20, "kJ/mol")
    KMM[1, 2] = Q_(1e-3, "M")
    KMM[2, 2] = Q_(1e-1, "M")

    # add a negative allosteric feedback from X1 to reaction 1
    A_inh[1, 1] = 2
    K_inh[1, 1] = Q_(2e-5, "M")

    # add a positive allosteric feedback from X1 to reaction 2
    A_act[1, 2] = 2
    K_act[1, 2] = Q_(1e-3, "M")

    ln_conc_lb = np.log(np.array([1e-4, 1e-6, 1e-4]))
    ln_conc_ub = np.log(np.array([1e-4, 1e-2, 1e-4]))
    ln_conc_confidence = 0.95

    return EnzymeCostFunction(
        S,
        fluxes=fluxes,
        kcat=kcat,
        standard_dg=standard_dg,
        KMM=KMM,
        ln_conc_lb=ln_conc_lb,
        ln_conc_ub=ln_conc_ub,
        ln_conc_confidence=ln_conc_confidence,
        A_act=A_act,
        A_inh=A_inh,
        K_act=K_act,
        K_inh=K_inh,
    )


def test_simulate(bistable_ecf):
    """Test by simulating the bistable model."""
    # TODO: fix this test
    pytest.skip()
    Nc, Nr = bistable_ecf.S.shape
    ln_c = np.log(1e-4) * np.ones(Nc)
    E = np.array([0.5, 0.225, 0.325])  # [g]

    simu = EnzymeCostSimulator(bistable_ecf)

    v_inf, lnC_inf = simu.Simulate(ln_c, E)

    assert v_inf == pytest.approx(0.0912, rel=0.01)
    assert lnC_inf[0] == pytest.approx(np.log(1e-4), rel=0.01)
    assert lnC_inf[1] == pytest.approx(-2.15, rel=0.01)
    assert lnC_inf[2] == pytest.approx(np.log(1e-4), rel=0.01)
