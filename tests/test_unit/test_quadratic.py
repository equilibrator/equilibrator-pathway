"""A module for testing the MDMC functions."""

# The MIT License (MIT)
#
# Copyright (c) 2013 Weizmann Institute of Science
# Copyright (c) 2018-2020 Institute for Molecular Systems Biology,
# ETH Zurich
# Copyright (c) 2018-2020 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
import pytest
from matplotlib import pyplot as plt
from sbtab import SBtab

from equilibrator_pathway import EnzymeCostModel, ThermodynamicModel


def test_sbtab_mdmc(test_dir, comp_contribution):
    """Test MDMC on the CCM model."""
    sbtabdoc = SBtab.read_csv(str(test_dir / "ccm_with_dgs.tsv"), "model")

    thermo_model = ThermodynamicModel.from_sbtab(
        sbtabdoc, comp_contrib=comp_contribution
    )
    mdmc_sol = thermo_model.mdmc_analysis()

    assert mdmc_sol.objective_values.iat[0] == pytest.approx(11.93, rel=0.01)
    assert mdmc_sol.objective_values.iat[-1] == pytest.approx(204.26, rel=0.01)

    assert mdmc_sol.zscore_df.loc[
        "3_Phospho_D_glyceroyl_phosphate", 0.0
    ] == pytest.approx(-1.475, rel=0.01)

    assert mdmc_sol.zscore_df.loc["Isocitrate", 10.0] == pytest.approx(-3.120, rel=0.01)
    rxn_df = mdmc_sol.reaction_prices_df(normalized=False)
    assert rxn_df.loc["ACN_R01325", 0.0] == pytest.approx(0.314, rel=0.01)
    assert rxn_df.loc["TIM_R01015", 9.9] == pytest.approx(5.08, rel=0.01)
    assert rxn_df.loc["MDH_R00342", 10.0] == pytest.approx(0.952, rel=0.01)

    # run the plotting code just to make sure it doesn't crash
    fig, axs = plt.subplots(1, 4)
    mdmc_sol.plot_reaction_prices(ax=axs[0], normalized=False)
    mdmc_sol.plot_reaction_prices(ax=axs[1], normalized=True)
    mdmc_sol.plot_concentrations(ax=axs[2], show_zscores=False)
    mdmc_sol.plot_concentrations(ax=axs[3], show_zscores=True)
    plt.close(fig)


def test_sbtab_ecma(test_dir, comp_contribution):
    """Test ECMA on the CCM model."""
    modeldata_fname = str(test_dir / "ccm_with_dgs.tsv")
    model = EnzymeCostModel.from_sbtab(modeldata_fname, comp_contrib=comp_contribution)
    df = model.pareto(weights=[0.0, 0.1, 0.2])

    costs = df[df.var_type == "obj"].pivot(
        index="weight", columns="name", values="value"
    )
    assert costs.at[0.0, "enzyme_cost"] == pytest.approx(79.5, rel=0.01)
    assert costs.at[0.2, "enzyme_cost"] == pytest.approx(97.1, rel=0.01)
    assert costs.at[0.1, "metabolic_adjustment"] == pytest.approx(20.1, rel=0.01)
    assert costs.at[0.1, "metabolite_weight"] == pytest.approx(29.5, rel=0.01)
    assert costs.at[0.2, "enzyme_weight"] == pytest.approx(68.7, rel=0.01)
