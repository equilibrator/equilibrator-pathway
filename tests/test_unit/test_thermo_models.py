"""A module for testing MDF."""

# The MIT License (MIT)
#
# Copyright (c) 2013 Weizmann Institute of Science
# Copyright (c) 2018-2020 Institute for Molecular Systems Biology,
# ETH Zurich
# Copyright (c) 2018-2020 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
import pytest
from matplotlib import pyplot as plt
from sbtab import SBtab

from equilibrator_pathway import ThermodynamicModel


@pytest.mark.parametrize(
    "sbtab_fname",
    [("ccm_without_dgs.tsv")],
)
def test_pathway(sbtab_fname, test_dir, comp_contribution):
    """Test MDF on the CCM model (without thermo data)."""
    sbtabdoc = SBtab.read_csv(str(test_dir / sbtab_fname), "model")

    thermo_model = ThermodynamicModel.from_sbtab(
        sbtabdoc, comp_contrib=comp_contribution
    )

    net_rxn = thermo_model.net_reaction
    assert net_rxn.get_stoichiometry(
        thermo_model.compound_dict["CO2"]
    ) == pytest.approx(0.546)

    mdf_sol = thermo_model.mdf_analysis()

    # test writing results to SBtab without errors
    _ = mdf_sol.to_sbtab().to_str()

    assert mdf_sol.score == pytest.approx(4.01, rel=0.01)

    sp = mdf_sol.reaction_df.set_index("reaction_id").shadow_price
    assert sp["FUM_R01082"] == pytest.approx(0.29, abs=1e-2)
    assert sp["MDH_R00342"] == pytest.approx(0.63, abs=1e-2)

    # run the plotting code just to make sure it doesn't crash
    fig, axs = plt.subplots(1, 2)
    mdf_sol.plot_concentrations(ax=axs[0])
    mdf_sol.plot_driving_forces(ax=axs[1])
    plt.close(fig)


@pytest.mark.parametrize(
    "sbtab_fname",
    [("ccm_with_dgs.tsv"), ("ccm_with_keq.tsv")],
)
def test_pathway_legacy(sbtab_fname, test_dir, comp_contribution):
    """Test MDF on the CCM model (which includes thermo data)."""
    sbtabdoc = SBtab.read_csv(str(test_dir / sbtab_fname), "model")

    thermo_model = ThermodynamicModel.from_sbtab(
        sbtabdoc, comp_contrib=comp_contribution
    )
    mdf_sol = thermo_model.mdf_analysis()

    # test writing results to SBtab without errors
    _ = mdf_sol.to_sbtab().to_str()

    assert mdf_sol.score == pytest.approx(2.07, rel=0.01)

    sp = mdf_sol.reaction_df.set_index("reaction_id").shadow_price
    assert sp["MDH_R00342"] == pytest.approx(1.0, abs=1e-5)

    # run the plotting code just to make sure it doesn't crash
    fig, axs = plt.subplots(1, 2)
    mdf_sol.plot_concentrations(ax=axs[0])
    mdf_sol.plot_driving_forces(ax=axs[1])
    plt.close(fig)
