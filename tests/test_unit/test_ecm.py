"""A module for testing the ECM classes."""

# The MIT License (MIT)
#
# Copyright (c) 2013 Weizmann Institute of Science
# Copyright (c) 2018-2020 Institute for Molecular Systems Biology,
# ETH Zurich
# Copyright (c) 2018-2020 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
import cvxpy as cp
import numpy as np
import pytest
from equilibrator_api import Q_, R, default_T
from matplotlib import pyplot as plt

from equilibrator_pathway import EnzymeCostFunction, EnzymeCostModel


def toy_ecf(ecf_version: int, objective: str) -> EnzymeCostFunction:
    """Create an a toy ECF."""
    Nr = 3
    Nc = 4
    S = np.zeros((Nc, Nr))

    S[0, 0] = -1
    S[1, 0] = 1
    S[2, 0] = 1
    S[1, 1] = -1
    S[2, 1] = 1
    S[2, 2] = -1
    S[3, 2] = 1

    fluxes = np.array([1.0, 1.0, 2.0], ndmin=2).T * Q_(1, "dimensionless")
    kcat = np.array([1.0, 1.0, 1.0], ndmin=2).T * Q_(1, "1/s")
    dGm_r = np.array([-3.0, -2.0, -3.0], ndmin=2).T * Q_(1, "kJ/mol")
    dG0_r = dGm_r - S.T @ np.ones((Nc, 1)) * R * default_T * np.log(1e-3)

    KMM = np.ones(S.shape) * Q_("M")
    KMM[S < 0] = Q_("90 mM")
    KMM[S > 0] = Q_("10 mM")
    lb = Q_("1 nM")
    ub = Q_("100 mM")

    ln_conc_lb = np.ones(Nc) * np.log(lb.m_as("M"))
    ln_conc_ub = np.ones(Nc) * np.log(ub.m_as("M"))

    A_act = np.zeros(S.shape)
    A_act[3, 0] = 1.0
    A_inh = np.zeros(S.shape)
    K_act = np.ones(S.shape) * Q_("M")
    K_act[3, 0] = Q_(5, "mM")
    K_inh = np.ones(S.shape) * Q_("M")
    ln_conc_confidence = 0.95

    params = {
        "flux_unit": "",
        "version": f"{ecf_version}",
        "kcat_source": "gmean",
        "denominator": "CM",
        "standard_concentration": "1 M",
        "objective": objective,
        "solver": cp.CLARABEL,
    }

    return EnzymeCostFunction(
        S,
        fluxes,
        kcat,
        dG0_r,
        KMM,
        ln_conc_lb=ln_conc_lb,
        ln_conc_ub=ln_conc_ub,
        ln_conc_confidence=ln_conc_confidence,
        mw_enz=None,
        mw_met=None,
        A_act=A_act,
        A_inh=A_inh,
        K_act=K_act,
        K_inh=K_inh,
        params=params,
    )


@pytest.mark.parametrize(
    "version, objective, expected_score, expected_conc",
    [
        (3, "enzyme", 12.22, [0.1, 0.015, 0.0067, 1e-9]),
        (3, "enzyme + metabolite", 12.34, [0.1, 0.015, 0.0067, 1e-9]),
        (4, "enzyme", 19.91, [0.1, 0.0119, 0.00774, 0.0045]),
        (4, "enzyme + metabolite", 20.03, [0.1, 0.0119, 0.00774, 0.0045]),
    ],
)
def test_toy_ecf(version, objective, expected_score, expected_conc):
    """Test the toy ECF."""
    ecf = toy_ecf(version, objective)
    ecm_score, ln_conc = ecf.optimize_ecm()
    conc = np.exp(ln_conc)
    assert ecm_score == pytest.approx(expected_score, rel=0.1)
    assert conc == pytest.approx(expected_conc, rel=0.1)


@pytest.mark.parametrize(
    "solver, raises_solver_error",
    [
        (
            "ECOS",
            False,
        ),
        (
            "CLARABEL",
            False,
        ),
        (
            "SCS",
            False,
        ),
        (
            "OSQP",
            True,
        ),
        (
            "SCIPY",
            True,
        ),
    ],
)
def test_sbtab(solver, raises_solver_error, test_dir, comp_contribution):
    """Test the ECM on a medium size model."""
    modeldata_fname = str(test_dir / "ccm_with_dgs.tsv")
    model = EnzymeCostModel.from_sbtab(modeldata_fname, comp_contrib=comp_contribution)
    model.ecf.params["solver"] = solver

    # Test correct Parameter loading
    idx_r = model.reaction_ids.index("PTS_RPTSsy")
    idx_c = model.compound_ids.index("Pyruvate")
    assert model.ecf.kcat[idx_r] == pytest.approx(38.7, rel=0.1)
    assert model.ecf.KMM[idx_c, idx_r] == pytest.approx(0.20e-3, rel=0.1)

    idx_r = model.reaction_ids.index("GAP_R01061")
    idx_c = model.compound_ids.index("NADplus")
    assert model.ecf.kcat[idx_r] == pytest.approx(70.74, rel=0.1)
    assert model.ecf.KMM[idx_c, idx_r] == pytest.approx(0.276e-3, rel=0.1)

    # Test ECM optimization
    if raises_solver_error:
        with pytest.raises(cp.SolverError):
            model.optimize_ecm()
    else:
        ecm_sol = model.optimize_ecm()

        assert ecm_sol.score == pytest.approx(79.48, rel=0.1)
        assert ecm_sol.enzyme_df.concentration_in_molar.sum() == pytest.approx(
            6.75e-4, rel=0.1
        )

        cid2conc_ecm = ecm_sol.compound_df.set_index(
            "compound_id"
        ).concentration_in_molar
        assert cid2conc_ecm["Glycerone_phosphate"] == pytest.approx(2.4e-3, rel=0.1)
        assert cid2conc_ecm["Acetyl_CoA"] == pytest.approx(1.5e-3, rel=0.1)
        assert cid2conc_ecm["D_Glucose"] == pytest.approx(12.0e-3, rel=0.1)

        rid2conc = ecm_sol.enzyme_df.set_index("reaction_id").concentration_in_molar
        assert rid2conc["ACN_R01900"] == pytest.approx(7.35e-5, rel=0.1)
        assert rid2conc["PGH_R00658"] == pytest.approx(5.22e-5, rel=0.1)
        assert rid2conc["GAP_R01061"] == pytest.approx(1.37e-4, rel=0.1)

        # run the plotting code just to make sure it doesn't crash
        fig, axs = plt.subplots(1, 4)
        ecm_sol.plot_enzyme_demand_breakdown(ax=axs[0])
        ecm_sol.plot_volumes(ax=axs[1])
        ecm_sol.plot_volumes_pie(ax=axs[2])
        ecm_sol.plot_thermodynamic_profile(ax=axs[3])
        plt.close(fig)


def test_validation_sbtab(test_dir, comp_contribution):
    """Test the validation procedures."""
    modeldata_fname = str(test_dir / "ecoli_noor_2016_ecm.tsv")
    validdata_fname = str(test_dir / "ecoli_noor_2016_reference.tsv")

    model = EnzymeCostModel.from_sbtab(modeldata_fname, comp_contrib=comp_contribution)
    model.add_validation_data(validdata_fname)

    ecm_sol = model.optimize_ecm()

    # test writing results to SBtab without errors
    _ = ecm_sol.to_sbtab().to_str()

    fig, axs = plt.subplots(1, 3)
    ecm_sol.plot_enzyme_demand_breakdown(ax=axs[0], plot_measured=True)
    ecm_sol.validate_metabolite_conc(ax=axs[1])
    ecm_sol.validate_enzyme_conc(ax=axs[2])
    plt.close(fig)
