"""A module for testing the network generating functions."""

# The MIT License (MIT)
#
# Copyright (c) 2013 Weizmann Institute of Science
# Copyright (c) 2018-2020 Institute for Molecular Systems Biology,
# ETH Zurich
# Copyright (c) 2018-2020 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import pytest
from path import Path
from sbtab import SBtab

from equilibrator_pathway import Bounds, Pathway


@pytest.fixture(scope="module")
def test_dir() -> Path:
    """Return the testing directory."""
    return Path(__file__).abspath().parent


def test_from_network_sbtab(test_dir, comp_contribution):
    """Test the functions that create a model from formula strings."""
    network_fname = str(test_dir / "fermentation_network_with_flux.tsv")
    bounds = Bounds.get_default_bounds(comp_contribution)
    pathway = Pathway.from_network_sbtab(
        network_fname,
        comp_contrib=comp_contribution,
        freetext=True,
        bounds=bounds,
    )

    assert (
        pathway.net_reaction_formula
        == "2 ADP + D_Glucose + 2 Phosphate = 2 ATP + 2 CO2 + 2 Ethanol + 2 H2O"
    )

    output_sbtab = pathway.to_sbtab()

    with open(str(test_dir / "fermentation_pathway_new.tsv"), "wt") as fp:
        fp.write(output_sbtab.to_str())

    expected_sbtab = SBtab.read_csv(str(test_dir / "fermentation_pathway.tsv"), "model")

    # this overrides a bug in the current version of sbtab (0.9.77) where
    # all tables get the "Date" attribute even though we only need one
    # in the SBtabDocument header.
    for sbtab in expected_sbtab.sbtabs:
        sbtab.unset_attribute("Date")

    assert output_sbtab.to_str() == expected_sbtab.to_str()
